const { app, BrowserWindow } = require('electron')


function createWindow () {
    // Cree la fenetre du navigateur.
    let win = new BrowserWindow({
        width: 700,
        height: 400,
        center : true,
        movable : false,
        title:"Electron Corentin",
        webPreferences: {
            nodeIntegration: true
        }
    })

    // and load the index.html of the app.
    win.loadFile('index.html')

}

app.on('ready', createWindow)


